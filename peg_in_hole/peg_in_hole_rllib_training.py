import ray
from ray.rllib.agents import ppo
import argparse
from peg_in_hole.peg_in_hole_gym_env import PegInHoleGymEnv
from peg_in_hole.peg_in_hole_callbacks import PegInHoleCallbacks

parser = argparse.ArgumentParser(description='Train robot on peg_in_hole with Rllib with PPO')
parser.add_argument('-gui', dest='gui', action="store_true", help='start with gui', default=False)
parser.add_argument('-lr', dest='learning_rate', type=float, help='learning rate for training', default=0.00005)
parser.add_argument('-d', dest='degrees_of_freedom', type=int, help='degrees_of_freedom', default=4)
parser.add_argument('-w', dest='num_workers', type=int, help='num_workers', default=1)
parser.add_argument('-n', dest='neurons_per_layer', type=int, help='neurons_per_layer', default=256)
parser.add_argument('-t', dest='training_steps', type=int, help='training_steps', default=500)
args = parser.parse_args()

ray.init()

config = ppo.DEFAULT_CONFIG.copy()
config["num_workers"] = args.num_workers
config["lr"] = args.learning_rate
config["observation_filter"] = "MeanStdFilter"
config["train_batch_size"] = 1000  # about 5 episodes per batch
config["env_config"] = {"visualization": args.gui, "max_time_step": 200, "degrees_of_freedom": args.degrees_of_freedom}
config["model"]["fcnet_hiddens"] = [args.neurons_per_layer, args.neurons_per_layer]

config["callbacks"] = PegInHoleCallbacks

trainer = ppo.PPOTrainer(env=PegInHoleGymEnv, config=config)

print("training_steps:" + str(args.training_steps))
for i in range(args.training_steps):
    trainer.train()
    if i % 10 == 0:
        checkpoint_path = trainer.save()
        print(checkpoint_path)
