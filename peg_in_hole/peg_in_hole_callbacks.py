from typing import Dict

from ray.rllib.env import BaseEnv
from ray.rllib.policy import Policy
from ray.rllib.evaluation import MultiAgentEpisode, RolloutWorker
from ray.rllib.agents.callbacks import DefaultCallbacks


class PegInHoleCallbacks(DefaultCallbacks):

    def on_episode_end(self, worker: RolloutWorker, base_env: BaseEnv,
                       policies: Dict[str, Policy], episode: MultiAgentEpisode,
                       **kwargs):
        final_height = episode.last_raw_obs_for()[0][5]
        print("episode {} ended with length {} and final height {}".format(
            episode.episode_id, episode.length, final_height))
        episode.custom_metrics["final_height"] = final_height
        episode.hist_data["final_height"] = final_height
