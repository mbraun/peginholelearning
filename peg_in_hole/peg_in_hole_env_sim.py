from pybullet_data import *
from peg_in_hole.pybullet_robot_sim import *
from peg_in_hole.perform_action_interface import PerformActionInterface

import numpy as np
import pybullet as p


class EnvPegInHole:

    def __init__(self, visualize=False, perform_action_strategy=None):
        self.__pos_offset_x = 0
        self.__pos_offset_y = 0
        self.__robot, self.__world = self.setup_environment(visualize)
        self.__perform_action_strategy = perform_action_strategy

    def get_robot(self):
        return self.__robot

    def get_world(self):
        return self.__world

    def step_simulation(self):
        p.stepSimulation()

    def set_perform_action_strategy(self, strategy: PerformActionInterface) -> None:
        self.__perform_action_strategy = strategy

    def reset_simulation(self):
        self.randomly_spawn_world()
        for i in range(7):
            p.resetJointState(self.__robot.robot_id, i, 0)
        self.__robot.init_robot_position()

    def randomly_spawn_world(self):
        p.removeBody(self.__world)
        self.__pos_offset_x = np.clip(np.random.normal(0, 0.001), -0.002, 0.002)
        self.__pos_offset_y = np.clip(np.random.normal(0, 0.001), -0.002, 0.002)
        print("x/y offset: " + str(self.__pos_offset_x) + "/" + str(self.__pos_offset_y))

        self.__world = p.loadURDF("peg_in_hole/ressources/urdf/environment/environment.urdf",
                                  basePosition=[0.55 + self.__pos_offset_x, 0 + self.__pos_offset_y, 0])

    def setup_environment(self, visualize):
        # initialize simulation environment
        if visualize:
            p.connect(p.GUI)
        else:
            p.connect(p.DIRECT)  # run without GUI
        p.resetSimulation()
        p.setAdditionalSearchPath(getDataPath())

        plane = p.loadURDF("plane.urdf")
        world = p.loadURDF("peg_in_hole/ressources/urdf/environment/environment.urdf",
                           basePosition=[0.55 + self.__pos_offset_x, 0 + self.__pos_offset_y, 0])

        # limit the force of the hinge to 0 so that it is movable
        mode = p.VELOCITY_CONTROL
        # p.setJointMotorControl2(world, 1, controlMode=mode, force=0)

        # set gravity and sim time step
        p.setGravity(0, 0, -9.81)  # everything should fall down
        p.setTimeStep(0.001)  # this slows everything down, but let's be accurate...
        p.setRealTimeSimulation(1)  # simulate faster real time :)

        robot = Robot("peg_in_hole/ressources/urdf/iiwa_description/urdf/iiwa14.urdf", verbose=False)
        robot.init_robot_position()

        return robot, world

    def stop_simulation(self):
        p.disconnect()

    def perform_robot_action(self, action):
        self.__perform_action_strategy.perform_action(self.__robot, action)
        self.step_simulation()
