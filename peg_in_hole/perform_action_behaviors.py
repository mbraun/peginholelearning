from peg_in_hole.perform_action_interface import PerformActionInterface
from peg_in_hole.pybullet_robot_sim import Robot


class PerformOneDimensionalAction(PerformActionInterface):
    def perform_action(self, robot: Robot, action):
        for i in range(20):
            if action == 0:
                robot.explicit_force_control(forces=[1, 0, -1])
            elif action == 1:
                robot.explicit_force_control(forces=[-1, 0, -1])
            elif action == 2:
                robot.explicit_force_control(forces=[0, 1, -1])
            elif action == 3:
                robot.explicit_force_control(forces=[0, -1, -1])
            elif action == 4:
                robot.explicit_force_control(forces=[0, 0, -1])
            elif action == 5:
                robot.pitch_up()
                robot.explicit_force_control(forces=[0, 0, -1])
            elif action == 6:
                robot.pitch_down()
                robot.explicit_force_control(forces=[0, 0, -1])


class PerformTwoDimAction(PerformActionInterface):
    def perform_action(self, robot: Robot, action):
        force_x = action[0]
        force_y = action[1]

        for i in range(20):
            robot.explicit_force_control(forces=[force_x, force_y, -1])


class PerformThreeDimAction(PerformActionInterface):
    def perform_action(self, robot: Robot, action):
        force_x = action[0]
        force_y = action[1]
        pitch = action[2]
        for i in range(20):
            if pitch == 1:
                robot.pitch_up()
            elif pitch == -1:
                robot.pitch_down()
            robot.explicit_force_control(forces=[force_x, force_y, -1])




class PerformFourDimAction(PerformActionInterface):
    def perform_action(self, robot: Robot, action):
        force_x = action[0]
        force_y = action[1]
        roll = action[2]
        pitch = action[3]

        for i in range(20):
            if roll == 1:
                robot.roll_up()
            elif roll == -1:
                robot.roll_down()
            if pitch == 1:
                robot.pitch_up()
            elif pitch == -1:
                robot.pitch_down()
            robot.explicit_force_control(forces=[force_x, force_y, -1])


class PerformFiveDimAction(PerformActionInterface):
    def perform_action(self, robot: Robot, action):
        force_x = action[0]
        force_y = action[1]
        roll = action[2]
        pitch = action[3]
        yaw = action[4]

        for i in range(20):
            if roll == 1:
                robot.roll_up()
            elif roll == -1:
                robot.roll_down()
            if pitch == 1:
                robot.pitch_up()
            elif pitch == -1:
                robot.pitch_down()
            if yaw == 1:
                robot.yaw_up()
            elif yaw == -1:
                robot.yaw_down()
            robot.explicit_force_control(forces=[force_x, force_y, -1])


class PerformSixDimAction(PerformActionInterface):
    def perform_action(self, robot: Robot, action):
        force_x = action[0]
        force_y = action[1]
        force_z = action[2]
        roll = action[3]
        pitch = action[4]
        yaw = action[5]

        for i in range(20):
            if roll == 1:
                robot.roll_up()
            elif roll == -1:
                robot.roll_down()
            if pitch == 1:
                robot.pitch_up()
            elif pitch == -1:
                robot.pitch_down()
            if yaw == 1:
                robot.yaw_up()
            elif yaw == -1:
                robot.yaw_down()
            robot.explicit_force_control(forces=[force_x, force_y, force_z])


