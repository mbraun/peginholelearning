import gym
from gym.spaces import Discrete, Box
import numpy as np
import random
from peg_in_hole.peg_in_hole_env_sim import EnvPegInHole
from peg_in_hole.perform_action_behaviors import *


class PegInHoleGymEnv(gym.Env):
    """Custom Env for PegInHole Pybullet Simulation"""

    def __init__(self, env_config):
        print(env_config)
        visualization = env_config["visualization"]
        degrees_of_freedom = env_config["degrees_of_freedom"]
        self.__env = EnvPegInHole(visualization)

        if degrees_of_freedom == 2:
            print("DoF = 2")
            self.__state_size = 14
            self.action_space = Box(low=-1, high=1, shape=(2,))
            self.observation_space = Box(high=10, low=-10, shape=(1, self.__state_size,), dtype=np.float32)
            self.__env.set_perform_action_strategy(PerformTwoDimAction())
        elif degrees_of_freedom == 3:
            print("DoF = 3")
            self.__state_size = 15
            self.action_space = Box(low=-1, high=1, shape=(3,))
            self.observation_space = Box(high=10, low=-10, shape=(1, self.__state_size,), dtype=np.float32)
            self.__env.set_perform_action_strategy(PerformThreeDimAction())
        elif degrees_of_freedom == 4:
            print("DoF = 4")
            self.__state_size = 16
            self.action_space = Box(low=-1, high=1, shape=(4,))
            self.observation_space = Box(high=10, low=-10, shape=(1, self.__state_size,), dtype=np.float32)
            self.__env.set_perform_action_strategy(PerformFourDimAction())
        elif degrees_of_freedom == 5:
            print("DoF = 5")
            self.__state_size = 17
            self.action_space = Box(low=-1, high=1, shape=(5,))
            self.observation_space = Box(high=10, low=-10, shape=(1, self.__state_size,), dtype=np.float32)
            self.__env.set_perform_action_strategy(PerformFiveDimAction())
        elif degrees_of_freedom == 6:
            print("DoF = 6")
            self.__state_size = 18
            self.action_space = Box(low=-1, high=1, shape=(6,))
            self.observation_space = Box(high=10, low=-10, shape=(1, self.__state_size,), dtype=np.float32)
            self.__env.set_perform_action_strategy(PerformSixDimAction())
        else:
            print("DoF doesn't match a defined configuration. Working with one dimensional action space")
            self.__state_size = 13
            self.action_space = Discrete(7)
            self.observation_space = Box(high=10, low=-10, shape=(1, self.__state_size,), dtype=np.float32)
            self.__env.set_perform_action_strategy(PerformOneDimensionalAction())

        self._episode_ended = False
        self._done = False
        self._score = 0
        self._time_step = 0
        self.__max_time = env_config["max_time_step"]
        self._state = []
        self.performance_buffer = []

    def get_robot_state(self):
        robot = self.__env.get_robot()
        state = [robot.get_transformed_eef_forces()[0], robot.get_transformed_eef_forces()[1],
                 robot.get_transformed_eef_forces()[2],
                 robot.get_eef_pos()[0], robot.get_eef_pos()[1], robot.get_eef_pos()[2],
                 robot.get_transformed_eef_torques()[0], robot.get_transformed_eef_torques()[1],
                 robot.get_transformed_eef_torques()[2],
                 robot.get_eef_rotation()[0], robot.get_eef_rotation()[1], robot.get_eef_rotation()[2]]
        state = np.clip(state, -10, 10)
        return state

    def reset(self):
        self.__env.reset_simulation()
        for i in range(20):
            self.__env.perform_robot_action([0, 0, -1, 0, 0, 0])
        self._episode_ended = False
        self._time_step = 0
        state = self.get_robot_state()
        state = np.append(state, np.zeros(self.action_space.shape))
        self._state = state
        return [self._state]

    def step(self, action):
        self._time_step += 1
        self.__env.perform_robot_action(action)
        next_state = self.get_robot_state()
        next_state = np.append(next_state, action.flatten())
        self._state = next_state
        reward = self.calculate_reward(next_state)

        done = False
        if self._time_step == self.__max_time:
            robot = self.__env.get_robot()
            self.performance_buffer.append(robot.get_eef_pos()[2])
            print("Final height: " + str(robot.get_eef_pos()[2]))
            done = True
        return [self._state], reward, done, {}

    def seed(self, seed=None):
        random.seed(seed)

    def calculate_reward(self, state):
        reward = 0.17 - self.__env.get_robot().get_eef_pos()[2]
        return reward
