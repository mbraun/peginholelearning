from abc import ABC, abstractmethod
from peg_in_hole.pybullet_robot_sim import Robot


class PerformActionInterface(ABC):

    @abstractmethod
    def perform_action(self, robot: Robot, action):
        pass
