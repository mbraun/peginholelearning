#!/usr/bin/python
import pybullet as p
import numpy as np
import math

class Robot:
    def __init__(self, path_to_urdf, verbose=False):
        # load the robot model and the box
        self.robot_id = p.loadURDF(path_to_urdf, [0, 0, 0], useFixedBase=1)  # use a fixed base!
        # enable force torque sensor for the ee_link
        p.enableJointForceTorqueSensor(self.robot_id, 9)
        self.joint_damping = None
        self.setup_joint_parameters()
        self.x_pos = None
        self.y_pos = None
        self.z_pos = None
        self.roll_angle = None
        self.pitch_angle = None
        self.yaw_angle = None

        self.__x_controller = None
        self.__y_controller = None
        self.__z_controller = None

    def setup_joint_parameters(self):
        self.joint_damping = 0.0000001
        self.joint_damping = [self.joint_damping, self.joint_damping, self.joint_damping, self.joint_damping,
                              self.joint_damping, self.joint_damping, self.joint_damping]

    def print_robot_info(self):
        print("--------------------------------------------------------------------------------------------")
        print("Robot Info")
        print(p.getNumJoints(self.robot_id))
        for i in range(10):
            print(p.getJointInfo(self.robot_id, i))

    def calc_inverse_kinematics(self, eef_pos, eef_rotation):
        orientation = p.getQuaternionFromEuler(eef_rotation)
        joint_pos = p.calculateInverseKinematics(self.robot_id, 9, eef_pos, orientation,
                                                 jointDamping=self.joint_damping)
        joint_pos = [0, joint_pos[0], joint_pos[1], 0, joint_pos[2], joint_pos[3], joint_pos[4], joint_pos[5]]
        return joint_pos

    def init_robot_position(self):
        p.resetJointState(self.robot_id, 0, 0)
        p.resetJointState(self.robot_id, 1, 0)
        p.resetJointState(self.robot_id, 2, 0.54)
        p.resetJointState(self.robot_id, 3, 0)
        p.resetJointState(self.robot_id, 4, -0.9)
        p.resetJointState(self.robot_id, 5, 0)
        p.resetJointState(self.robot_id, 6, 1.78)
        p.resetJointState(self.robot_id, 7, 0)
        p.setJointMotorControlArray(self.robot_id, range(8), p.POSITION_CONTROL,
                                    targetPositions=self.calc_inverse_kinematics((0.3, 0.0, 0.6), (0, -math.pi, 0)))
        p.setRealTimeSimulation(0)
        for i in range(10):
            p.stepSimulation()
        p.setJointMotorControlArray(self.robot_id, range(8), p.POSITION_CONTROL,
                                    targetPositions=self.calc_inverse_kinematics((0.55, 0.0, 0.18), (0, -math.pi, 0)))

        self.x_pos = 0.55
        self.y_pos = 0
        self.z_pos = 0.18
        self.roll_angle = 0
        self.pitch_angle = math.pi
        self.yaw_angle = 0

        p.setRealTimeSimulation(1)
        for i in range(10):
            p.stepSimulation()

    def explicit_force_control(self, forces=[0, 0, 0]):
        joint_reaction_forces = self.get_transformed_eef_forces()
        gain = 0.00004
        x_pos = self.x_pos + np.clip(((forces[0] - joint_reaction_forces[0]) * gain),
                                     -0.00005,
                                     0.00005)
        y_pos = self.y_pos + np.clip(((forces[1] - joint_reaction_forces[1]) * gain),
                                     -0.00005,
                                     0.00005)
        delta_z = np.clip(((forces[2] - joint_reaction_forces[2]) * gain), -0.0001, 0.0001)
        z_pos = self.z_pos + delta_z
        target_positions = self.calc_inverse_kinematics((x_pos, y_pos, z_pos),
                                                        (self.roll_angle, self.pitch_angle, self.yaw_angle))
        p.setJointMotorControlArray(self.robot_id, range(8), p.POSITION_CONTROL, targetPositions=target_positions)
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.z_pos = z_pos

    def mat33_from_quaternion(self, quaternions):
        mat33 = p.getMatrixFromQuaternion(quaternions)
        return np.reshape(mat33, [3, 3])

    def transform_forces(self, forces):
        matrix = self.mat33_from_quaternion(p.getLinkState(self.robot_id, 9)[1])
        forces = np.asarray(forces)
        forces = matrix.dot(forces)
        return forces

    def roll_down(self):
        self.roll_angle -= 0.0005

    def roll_up(self):
        self.roll_angle += 0.0005

    def pitch_down(self):
        self.pitch_angle -= 0.0005

    def pitch_up(self):
        self.pitch_angle += 0.0005

    def yaw_down(self):
        if self.get_transformed_eef_torques()[2] >= -2:
            self.yaw_angle -= 0.002
        else:
            self.yaw_angle += 0.001

    def yaw_up(self):
        if self.get_transformed_eef_torques()[2] <= 2:
            self.yaw_angle += 0.002
        else:
            self.yaw_angle -= 0.001

    def get_transformed_eef_forces(self):
        pos, vel, joint_reaction_forces, torque = p.getJointState(self.robot_id, 9)
        joint_reaction_forces = self.transform_forces(joint_reaction_forces[:3])
        return joint_reaction_forces

    def get_transformed_eef_torques(self):
        pos, vel, joint_reaction_forces, torque = p.getJointState(self.robot_id, 9)
        joint_reaction_forces = self.transform_forces(joint_reaction_forces[3:])
        return joint_reaction_forces

    def get_eef_pos(self):
        pos = p.getLinkState(self.robot_id, 9)[0]
        return pos

    def get_eef_rotation(self):
        rot = (self.roll_angle, self.pitch_angle, self.yaw_angle)
        return rot

    def get_eef_quaternions(self):
        return p.getQuaternionFromEuler(self.get_eef_rotation())

