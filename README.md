![peg_in_hole.png](./peg_in_hole.png)



The Reinforcement Learning (RL) framework provides a possibility to autonomous learning based on interaction with the environment but although much research has been done, poor trial efficiency is a problem for learning-based methods. Learning robust strategies requires many costly interactions with the environment, which severely limits the potential applications in an industrial context.
We propose a grey-box learning approach that allows process experts to provide a partial behavioral description based on the Task Frame Formalism. The source code provided includes a simulated peg-in-hole environment.

## Run Peg-in-Hole Training

For running the experiments just clone the git repo, cd into it and run:

python3 -m peg_in_hole.peg_in_hole_rllib_training -gui
